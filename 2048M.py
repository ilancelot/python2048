#encoding: utf-8
#2048M - 2048 Improved
#i@ilancelot.com
from graphics import *  #图形基本类库
from button import *    #基本按钮类库，做了基本的美化修改
import thread           #多线程类库，播放声音
import random           #随机类库，产生随机变量
import time             #时间类库，增强游戏互动性
#import winsound,sys    #由于不允许使用其他的库，所以播放声音的部分仅在代码演示中以注释形式展现。

class Row(): #2048中基本的一行定义为一类
	"""docstring for Row"""#对类的描述
	def __init__(self,rnum,n1,n2,n3,n4,win_handle): #初始化一行，同时把一行数据统一放在list里便于调用
		self.n1 = n1 
		self.n2 = n2
		self.n3 = n3
		self.n4 = n4
		self.numlist = [self.n1,self.n2,self.n3,self.n4]
		self.rnum = rnum
	def delZero(self,direction):#删除行中的零，对齐数据
		numofzero = self.numlist.count(0)
		for i in range(numofzero):
			self.numlist.remove(0)
		appendZerolist = []
		for i in range(numofzero):
			appendZerolist.append(0)
		if direction == 'left':
			self.numlist.extend(appendZerolist)
			self.n1,self.n2,self.n3,self.n4 = self.numlist

		elif direction == 'right':
			self.numlist[:0] = appendZerolist
			self.n1,self.n2,self.n3,self.n4 = self.numlist
		else:
			pass
	def findSame(self,direction):#找相同的数字，一开始觉得鸡肋，最后判断胜负的时候重要
		if direction == 'left':
			for i in [0,1,2]:
				if self.numlist[i] == self.numlist[i+1] and self.numlist[i] != 0 and self.numlist[i+1] != 0:
					return True
			return False
		if direction == 'right':
			for i in [3,2,1]:
				if self.numlist[i] == self.numlist[i-1] and self.numlist[i] != 0 and self.numlist[i-1] != 0:
					return True
			return False
	def addSameNumber(self,direction):#核心，遍历寻找相邻的相同数字，叠加
		tempScore = 0 #临时分数
		if direction == 'left':
			for i in [0,1,2]:
				if self.numlist[i] == self.numlist[i+1] and self.numlist[i] != 0 and self.numlist[i+1] != 0:
					self.numlist[i] = self.numlist[i] * 2
					self.numlist[i+1] = 0
					self.n1,self.n2,self.n3,self.n4 = self.numlist
					self.numlist = [self.n1,self.n2,self.n3,self.n4]
					tempScore += self.numlist[i]
		elif direction == 'right':
			for i in [3,2,1]:
				if self.numlist[i] == self.numlist[i-1] and self.numlist[i] != 0 and self.numlist[i-1] != 0:
					self.numlist[i-1] = self.numlist[i-1]*2
					self.numlist[i] = 0
					self.n1,self.n2,self.n3,self.n4 = self.numlist
					self.numlist = [self.n1,self.n2,self.n3,self.n4]
					tempScore += self.numlist[i]
		else:
			pass
	def rChange(self,n1,n2,n3,n4,rnum):#根据传递进来的参数直接修改Row内的数据
		self.n1 = n1
		self.n2 = n2
		self.n3 = n3
		self.n4 = n4
		self.numlist = [self.n1,self.n2,self.n3,self.n4]
		self.rnum = rnum
	def superChange(self,n,value):#修改Row指定位置的数据
		if n == 1:
			self.n1 = value
			self.numlist = [self.n1,self.n2,self.n3,self.n4]
		elif n == 2:
			self.n2 = value
			self.numlist = [self.n1,self.n2,self.n3,self.n4]
		elif n ==3:
			self.n3 = value
			self.numlist = [self.n1,self.n2,self.n3,self.n4]
		elif n == 4:
			self.n4 = value
			self.numlist = [self.n1,self.n2,self.n3,self.n4]
		else:
			pass
	def ChangeFromlist(self,datalist): #从一个列表里读取数据并改变相应的值
		self.n1 = datalist[0]
		self.n2 = datalist[1]
		self.n3 = datalist[2]
		self.n4 = datalist[3]
		self.numlist = [self.n1,self.n2,self.n3,self.n4]
	def rShow(self,win_handle): #在窗口画出相应方块
		self.c1 = cube(self.n1,posGen(self.rnum,1),win_handle)
		self.c2 = cube(self.n2,posGen(self.rnum,2),win_handle)
		self.c3 = cube(self.n3,posGen(self.rnum,3),win_handle)
		self.c4 = cube(self.n4,posGen(self.rnum,4),win_handle)
		self.c1.cDraw(win_handle)
		self.c2.cDraw(win_handle)
		self.c3.cDraw(win_handle)
		self.c4.cDraw(win_handle)
	def genlist(self): #返回一行内的值
		return [self.n1,self.n2,self.n3,self.n4]
	def execute(self,direction):#根据给定的方向移动一行
		self.delZero(direction)
		while self.findSame(direction) == True:
			self.addSameNumber(direction)
			self.delZero(direction)
class cube(): #小方块类
	"""docstring for cube"""
	def __init__(self, number,center,win_handle):
		self.number = number#代表的数字
		self.center = center#中心数字
	def cDraw(self,win_handle): #根据相应数字显示图片

		#self.f = Image(Point(xVal,yVal),filename)
		if   self.number == 0:
		  	self.img = Image(self.center,'0.gif'   )
		  	self.img.draw(win_handle)
		elif self.number == 2:
			self.img = Image(self.center,'2.gif'   )
			self.img.draw(win_handle)
		elif self.number == 4:
			self.img = Image(self.center,'4.gif'   )
			self.img.draw(win_handle)
		elif self.number == 8:	
			self.img = Image(self.center,'8.gif'   )
			self.img.draw(win_handle)
		elif self.number == 16:
			self.img = Image(self.center,'16.gif'  )
			self.img.draw(win_handle)
		elif self.number == 32:
			self.img = Image(self.center,'32.gif'  )
			self.img.draw(win_handle)
		elif self.number == 64:
			self.img = Image(self.center,'64.gif'  )
			self.img.draw(win_handle)
		elif self.number == 128:
			self.img = Image(self.center,'128.gif' )
			self.img.draw(win_handle)
		elif self.number == 256:
			self.img = Image(self.center,'256.gif' )
			self.img.draw(win_handle)
		elif self.number == 512:
			self.img = Image(self.center,'512.gif' )
			self.img.draw(win_handle)
		elif self.number == 1024:
			self.img = Image(self.center,'1024.gif')
			self.img.draw(win_handle)
		elif self.number == 2048:
			self.img = Image(self.center,'2048.gif')
			self.img.draw(win_handle)
	def cVanish(self):#消失图片
		self.img.undraw(win_handle)
def main():
	stepdata = []#后悔上一步
	win_handle = GraphWin("2048M",500,700)#画出主窗口
	rowlist = initgame(win_handle)#初始化游戏
	#regret_button = Button(win_handle,Point(200,430),100,50,"Regret")
	regretButton = Button(win_handle,Point(400,570),70,40,"Regret")  #
	resetButton  = Button(win_handle,Point(400,620),70,40,"Reset")   #
	quitButton   = Button(win_handle,Point(400,670),70,40,"Quit")    #
	upButton     = Button(win_handle,Point(200,570),70,40,"Up")      #
	downButton   = Button(win_handle,Point(200,670),70,40,"Down")    #
	leftButton   = Button(win_handle,Point(100,620),70,40,"Left")    #
	rightButton  = Button(win_handle,Point(300,620),70,40,"Right")   #
	#
	regretButton.activate()
	resetButton.activate()
	quitButton.activate()
	upButton.activate()
	downButton.activate()
	leftButton.activate()
	rightButton.activate()
	#
	gameloop = True
	for example in rowlist:
		example.rShow(win_handle)
	#
	while gameloop == True:
		p = win_handle.getMouse()#
		if quitButton.clicked(p):
			win_handle.close()
		elif resetButton.clicked(p):
			rowlist = initgame(win_handle)
			for example in rowlist:
				example.rShow(win_handle)
		elif regretButton.clicked(p):
			print 'clicked'
			rowlist,stepdata = regret(rowlist,stepdata,win_handle)
			for example in rowlist:
				example.rShow(win_handle)
		elif leftButton.clicked(p):
			stepdata = savegame(rowlist,stepdata )
			for i in range(0,4):
				rowlist[i].execute('left')
				rowlist[i].rShow(win_handle)
			print rowlist[i].genlist()

			if checkGame(rowlist,win_handle) == 'win':
				print 'win'
				wingame()
				time.sleep(3)
				rowlist = initgame(win_handle)
				for example in rowlist:
					example.rShow(win_handle)
			elif checkGame(rowlist,win_handle) == 'lose':
				print 'lose1'
				losegame()
				time.sleep(3)
				rowlist = initgame(win_handle)
				for example in rowlist:
					example.rShow(win_handle)
			else:
				addRandomNum(rowlist)
				show(rowlist,win_handle)
				if checkGame(rowlist,win_handle) == 'lose':
					print 'lose2'
					losegame()
					time.sleep(3)
					rowlist = initgame(win_handle)
					for example in rowlist:
						example.rShow(win_handle)
				else:
					pass
			
			print stepdata
		elif rightButton.clicked(p):
			stepdata = savegame(rowlist,stepdata )
			for i in range(0,4):
				rowlist[i].execute('right')
				rowlist[i].rShow(win_handle)
			if checkGame(rowlist,win_handle) == 'win':
				print 'win'
				wingame()
				time.sleep(3)
				rowlist = initgame(win_handle)
				for example in rowlist:
					example.rShow(win_handle)
			elif checkGame(rowlist,win_handle) == 'lose':
				print 'lose1'
				losegame()
				time.sleep(3)
				rowlist = initgame(win_handle)
				for example in rowlist:
					example.rShow(win_handle)
			else:
				addRandomNum(rowlist)
				show(rowlist,win_handle)
				for example in rowlist:
					print example.genlist()
				if checkGame(rowlist,win_handle) == 'lose':
					print 'lose2'
					losegame()
					time.sleep(3)
					rowlist = initgame(win_handle)
					for example in rowlist:
						example.rShow(win_handle)
				else:
					pass
			
		elif upButton.clicked(p):
			temp_rowlist = []
			r1 = rowlist[0].genlist()
			r2 = rowlist[1].genlist()
			r3 = rowlist[2].genlist()
			r4 = rowlist[3].genlist()
			tmp_r1 = Row(1,r1[0],r2[0],r3[0],r4[0],win_handle)
			tmp_r2 = Row(2,r1[1],r2[1],r3[1],r4[1],win_handle)
			tmp_r3 = Row(3,r1[2],r2[2],r3[2],r4[2],win_handle)
			tmp_r4 = Row(4,r1[3],r2[3],r3[3],r4[3],win_handle)
			temp_rowlist = [tmp_r1,tmp_r2,tmp_r3,tmp_r4]
			#up is left
			##########
			for example in temp_rowlist:
				print example.genlist()
				#########
			for i in range(0,4):
				temp_rowlist[i].execute('left')
			if checkGame(temp_rowlist,win_handle) == 'win':
				wingame()
				time.sleep(3)
				rowlist = initgame(win_handle)
				for example in rowlist:
						example.rShow(win_handle)
				pass
			elif checkGame(temp_rowlist,win_handle) == 'lose':
				losegame()
				time.sleep(3)
				rowlist = initgame(win_handle)
				for example in rowlist:
						example.rShow(win_handle)
			else:
				##########
				for example in temp_rowlist:
					print example.genlist()
				#########
				nr1 = []
				nr2 = []
				nr3 = []
				nr4 = []
				nr1 = temp_rowlist[0].genlist()
				nr2 = temp_rowlist[1].genlist()
				nr3 = temp_rowlist[2].genlist()
				nr4 = temp_rowlist[3].genlist()
				rowlist[0].rChange(nr1[0],nr2[0],nr3[0],nr4[0],1)
				rowlist[1].rChange(nr1[1],nr2[1],nr3[1],nr4[1],2)
				rowlist[2].rChange(nr1[2],nr2[2],nr3[2],nr4[2],3)
				rowlist[3].rChange(nr1[3],nr2[3],nr3[3],nr4[3],4)
				##########
				for example in rowlist:
					print example.genlist()
				#########
				addRandomNum(rowlist)
				if checkGame(rowlist,win_handle) == 'lose':
					losegame()
					time.sleep(3)
					rowlist = initgame(win_handle)
					for example in rowlist:
						example.rShow(win_handle)
				else:
					for example in rowlist:
						example.rShow(win_handle)
		elif downButton.clicked(p):
			temp_rowlist = []
			r1 = rowlist[0].genlist()
			r2 = rowlist[1].genlist()
			r3 = rowlist[2].genlist()
			r4 = rowlist[3].genlist()
			tmp_r1 = Row(1,r1[0],r2[0],r3[0],r4[0],win_handle)
			tmp_r2 = Row(2,r1[1],r2[1],r3[1],r4[1],win_handle)
			tmp_r3 = Row(3,r1[2],r2[2],r3[2],r4[2],win_handle)
			tmp_r4 = Row(4,r1[3],r2[3],r3[3],r4[3],win_handle)
			temp_rowlist = [tmp_r1,tmp_r2,tmp_r3,tmp_r4]
			#up is left
			for i in range(0,4):
				temp_rowlist[i].execute('right')
			if checkGame(temp_rowlist,win_handle) == 'win':
				wingame()
				time.sleep(3)
				rowlist = initgame(win_handle)
				for example in rowlist:
					example.rShow(win_handle)
				pass
			elif checkGame(temp_rowlist,win_handle) == 'lose':
				losegame()
				time.sleep(3)
				rowlist = initgame(win_handle)
				for example in rowlist:
					example.rShow(win_handle)
				pass
			else:
				##########
				for example in temp_rowlist:
					print example.genlist()
				#########
				nr1 = []
				nr2 = []
				nr3 = []
				nr4 = []
				nr1 = temp_rowlist[0].genlist()
				nr2 = temp_rowlist[1].genlist()
				nr3 = temp_rowlist[2].genlist()
				nr4 = temp_rowlist[3].genlist()
				rowlist[0].rChange(nr1[0],nr2[0],nr3[0],nr4[0],1)
				rowlist[1].rChange(nr1[1],nr2[1],nr3[1],nr4[1],2)
				rowlist[2].rChange(nr1[2],nr2[2],nr3[2],nr4[2],3)
				rowlist[3].rChange(nr1[3],nr2[3],nr3[3],nr4[3],4)
				##########
				for example in rowlist:
					print example.genlist()
				#########
				addRandomNum(rowlist)
				if checkGame(rowlist,win_handle) == 'lose':
					losegame()
					time.sleep(3)
					rowlist = initgame(win_handle)
					for example in rowlist:
						example.rShow(win_handle)
				else:
					for example in rowlist:
						example.rShow(win_handle)
		else:
			pass#主函数
def initgame(win_handle):
	row1 = Row(1,random.choice([0,2,4]),0,0,0,win_handle)
	row2 = Row(2,0,0,0,0,win_handle)
	row3 = Row(3,random.choice([0,2,4]),0,0,0,win_handle)
	row4 = Row(4,0,0,0,random.choice([0,2,4]),win_handle)
	rowlist = [row1,row2,row3,row4]
	for i in range(0,4):
		print rowlist[i].genlist()
	return rowlist#初始化
def posGen(x,y):
	if y == 1:
		if x ==1:
			return Point(100,150)
		elif x == 2:
			return Point(100,250)
		elif x == 3:
			return Point(100,350)
		elif x == 4:
			return Point(100,450)
	elif y == 2:
		if x == 1:
			return Point(200,150)
		elif x ==2:
			return Point(200,250)
		elif x == 3:
			return Point(200,350)
		elif x == 4:
			return Point(200,450)
	elif y == 3:
		if x == 1:
			return Point(300,150)
		elif x == 2:
			return Point(300,250)
		elif x == 3:
			return Point(300,350)
		elif x == 4:
			return Point(300,450)		
	elif y == 4:
		if x == 1:
			return Point(400,150)
		elif x == 2:
			return Point(400,250)
		elif x == 3:
			return Point(400,350)
		elif x == 4:
			return Point(400,450)#产生坐标的一个打酱油的函数
def checkGame(rowlist,win_handle):#如果相邻数字有重复，游戏继续
	
	for example in rowlist:
		if example.findSame('left'):
			return True
		if example.findSame('right'):
			return True
	for example in refactor(rowlist,win_handle):
		if example.findSame('left'):
			return True
		if example.findSame('right'):
			return True

	zero = 0
	for example in rowlist:
		for num in example.genlist():
			if num == 2048:
				return 'win'
	for example in rowlist:
		for num in example.genlist():
			if num == 0:
				zero += 1

	if zero == 0:
		return 'lose'
	else:
		return True
def addRandomNum(rowlist):#在这个矩阵里随意产生数字
	r1 = rowlist[0].genlist()
	r2 = rowlist[1].genlist()
	r3 = rowlist[2].genlist()
	r4 = rowlist[3].genlist()
	ranlist = []
	p = 0
	for num in r1:
		p = p + 1
		if num == 0:
			ranlist.append((p,1))
	p = 0 
	for num in r2:
		p = p + 1
		if num == 0:
			ranlist.append((p,2))
	p = 0 
	for num in r3:
		p = p + 1
		if num == 0:
			ranlist.append((p,3))
	p = 0 
	for num in r4:
		p = p + 1
		if num == 0:
			ranlist.append((p,4))

	if ranlist != []:
		allnum = len(ranlist)
		gennum = 3
		while gennum >= 0:
			allnum = len(ranlist)
			for (x,y) in ranlist[:1]:
				if y == 1:
					rowlist[0].superChange(x,random.choice([2,4]))
				elif y == 2:
					rowlist[1].superChange(x,random.choice([2,4]))
				elif y == 3:
					rowlist[2].superChange(x,random.choice([2,4]))
				elif y == 4:
					rowlist[3].superChange(x,random.choice([2,4]))
				else:
					pass
				gennum = gennum - 1
def losegame():
	win_handle_lose = GraphWin("Sorry!",100,100)
	time.sleep(3)
	win_handle_lose.close()#弹出输窗口 
def wingame():
	win_handle_win = GraphWin("You win!",100,100)
	time.sleep(3)
	win_handle_win.close()#弹出赢窗口
def savegame(rowlist,stepdata):
	stepdata = []
	for i in range(0,4):
		stepdata.append(rowlist[i].genlist())#保存上一步,call ChangeFromlist方法
	return stepdata
def regret(rowlist,stepdata,win_handle):
	if stepdata == [] or stepdata == None:
		return rowlist,None
	rowlist[0].ChangeFromlist(stepdata[0])
	rowlist[1].ChangeFromlist(stepdata[1])
	rowlist[2].ChangeFromlist(stepdata[2])
	rowlist[3].ChangeFromlist(stepdata[3])
	for example in rowlist:
		example.rShow(win_handle)#后悔，读取list数据调用
	return rowlist,[]
#def playsound(file_handle):
def show(rowlist,win_handle):#画矩阵
	for example in rowlist:
		example.rShow(win_handle)
def refactor(rowlist,win_handle): #转置行列式。。。。。。  _(:з」∠)_
	r1 = rowlist[0].genlist()
	r2 = rowlist[1].genlist()
	r3 = rowlist[2].genlist()
	r4 = rowlist[3].genlist()
	tmp_r1 = Row(1,r1[0],r2[0],r3[0],r4[0],win_handle)
	tmp_r2 = Row(2,r1[1],r2[1],r3[1],r4[1],win_handle)
	tmp_r3 = Row(3,r1[2],r2[2],r3[2],r4[2],win_handle)
	tmp_r4 = Row(4,r1[3],r2[3],r3[3],r4[3],win_handle)
	temp_rowlist = [tmp_r1,tmp_r2,tmp_r3,tmp_r4]
	return temp_rowlist
main()
